import { Component, OnInit } from '@angular/core';
import { ProductServicesService } from '../product-services.service'
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  productsList;
  constructor(private prd:ProductServicesService) { }

  ngOnInit() {
    this.productsList = this.prd.products;
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductComponent} from './product/product.component';
import { ProductDetailComponent } from './product-detail/product-detail.component'
import { ConfirmComponent } from './confirm/confirm.component'
import { CheckoutComponent } from './checkout/checkout.component'


const routes: Routes = [
  {path:'',component:ProductComponent},
  {path:'product-detail/:id',component:ProductDetailComponent},
  {path:'product-detail/checkout/:id',component:CheckoutComponent, pathMatch: 'full'},
  {path:'product-detail/checkout/confirm/:id',component:ConfirmComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  
 }

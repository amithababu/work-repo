import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerServicesService {
  private checkoutform;
  constructor() { }
  updateCheckoutForm(checkoutform:any){
    console.log("updateCheckoutForm")
    this.checkoutform = checkoutform;
  }
  getCheckoutForm():Observable<any>{
    console.log("getCheckoutForm")
    return this.checkoutform;
  }
  
}

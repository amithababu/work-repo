import { Component, OnInit } from '@angular/core';
import { CustomerServicesService } from '../customer-services.service';

import { ProductServicesService } from '../product-services.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent implements OnInit {
  users;
  productsList;
  productId;
  selectedProduct;
  constructor(private customerService: CustomerServicesService,
    private prd:ProductServicesService,
    private route: ActivatedRoute) {
   
   }

  ngOnInit() { 
    this.users = this.customerService.getCheckoutForm();
    console.log( this.users)
    this.productsList = this.prd.products;
    this.productId = +this.route.snapshot.paramMap.get('id');
    this.productsList.forEach(product => {
      if (product.id === this.productId) {
        this.selectedProduct = product;
      }
    });
  }

}

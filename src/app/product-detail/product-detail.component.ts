import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ProductServicesService } from '../product-services.service'


@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  productsList;
  productId;
  selectedProduct;

  constructor(private route: ActivatedRoute,
              private prd:ProductServicesService
    ) { }

  ngOnInit() {
    this.productsList = this.prd.products;
    this.productId = +this.route.snapshot.paramMap.get('id');
    this.productsList.forEach(product => {
      if (product.id === this.productId) {
        this.selectedProduct = product;
      }
    });
  }

}

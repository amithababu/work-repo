import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductServicesService {

  constructor() { }
  
  products = [
    {
      id: 0,
      name: "iPhone 6S",
      description: "Kogi skateboard tattooed, whatever portland fingerstache coloring book mlkshk leggings flannel dreamcatcher.",
      imageUrl: "./assets/images/food-1.jpg",
      price: 799
    },
    {
      id: 1,
      name: "iPhone 5S",
      description: "Kogi skateboard tattooed, whatever portland fingerstache coloring book mlkshk leggings flannel dreamcatcher.",
      imageUrl: "./assets/images/food-1.jpg",
      price: 349,
    },
    {
      id: 2,
      name: "Macbook",
      description: "Kogi skateboard tattooed, whatever portland fingerstache coloring book mlkshk leggings flannel dreamcatcher.",
      imageUrl: "./assets/images/food-1.jpg",
      price: 1499
    },
    {
      id: 3,
      name: "Macbook Air",
      description: "Kogi skateboard tattooed, whatever portland fingerstache coloring book mlkshk leggings flannel dreamcatcher.",
      imageUrl: "./assets/images/food-1.jpg",
      price: 999
    },
    {
      id: 4,
      name: "Macbook Air 2013",
      description: "Kogi skateboard tattooed, whatever portland fingerstache coloring book mlkshk leggings flannel dreamcatcher.",
      imageUrl: "./assets/images/food-1.jpg",
      price: 599
    },
    {
      id: 5,
      name: "Macbook Air 2012",
      description: "Kogi skateboard tattooed, whatever portland fingerstache coloring book mlkshk leggings flannel dreamcatcher.",
      imageUrl: "./assets/images/food-1.jpg",
      price: 499
    }
  ]
}

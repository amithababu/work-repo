import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { ProductServicesService } from '../product-services.service';
import { FormBuilder, FormGroup ,Validators } from '@angular/forms';
import { Router } from  '@angular/router';
import { CustomerServicesService } from '../customer-services.service';
import { from } from 'rxjs';
@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
 
  productsList;
  productId;
  selectedProduct;
  selctedPrdName;
  checkoutForms : FormGroup;
  submitted:boolean = false;
  success:boolean = false;
  constructor(private route: ActivatedRoute,
              private prd:ProductServicesService,
              private formBuilder : FormBuilder,
              private router: Router,
              private customerService: CustomerServicesService
    ) {
     
    }

  ngOnInit() {
    this.productsList = this.prd.products;
    this.productId = +this.route.snapshot.paramMap.get('id');
    this.productsList.forEach(product => {
      if (product.id === this.productId) {
        this.selectedProduct = product;
      }
    });
    this.checkoutForms = this.formBuilder.group({
      firstName : ['',Validators.required],
      lastName : ['',Validators.required],
      mail : ['',[Validators.required,Validators.email]],
      deliverydate: ['']
    })
  }
  get formControls() { return this.checkoutForms.controls; }
  onSubmit(){
    var formsval=this.checkoutForms.value
    console.log(formsval);
     this.submitted = true;
     if(this.checkoutForms.invalid){
       console.log("errrrrrror")
       return;
     }
     
      this.customerService.updateCheckoutForm(formsval);
    this.success =true; 
     this.router.navigate(['product-detail/checkout/confirm/',this.route.snapshot.paramMap.get('id')]);
    
     
   }
}
